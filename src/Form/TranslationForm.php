<?php

namespace Drupal\auto_block_translate\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\block_content\Entity\BlockContent;
use Drupal\auto_node_translate\Form\TranslationForm as NodeTranslationForm;

/**
 * The Translation Form.
 */
class TranslationForm extends NodeTranslationForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'auto_block_translate_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $block_content = NULL) {
    $languages = \Drupal::languageManager()->getLanguages();
    $form['translate'] = [
      '#type' => 'fieldgroup',
      '#title' => $this->t('Languages to Translate'),
      '#closed' => FALSE,
      '#tree' => TRUE,
    ];

    foreach ($languages as $language) {
      $languageId = $language->getId();
      if ($languageId !== $block_content->langcode->value) {
        $label = ($block_content->hasTranslation($languageId)) ? $this->t('overwrite translation') : $this->t('new translation');
        $form['translate'][$languageId] = [
          '#type' => 'checkbox',
          '#title' => $this->t('@lang (@label)', [
            '@lang' => $language->getName(),
            '@label' => $label,
          ]),
        ];
      }
    }
    $form['save'] = [
      '#type' => 'submit',
      '#value' => $this->t('Translate'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $config = \Drupal::config('auto_node_translate.config');
    if (empty($config->get('default_api'))) {
      $form_state->setError($form['translate'], $this->t('Error, translation API is not configured!'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $route = \Drupal::routeMatch();
    $block_content = $route->getParameter('block_content');
    $translations = $form_state->getValues()['translate'];
    $this->autoBlockTranslateBlock($block_content, $translations);

    $form_state->setRedirect('entity.block_content.canonical', ['block_content' => $block_content->id()]);
  }

  /**
   * Translates block_content.
   *
   * @param \Drupal\block_content\Entity\BlockContent $block_content
   *   The block_content to translate.
   * @param mixed $translations
   *   The translations array.
   */
  public function autoBlockTranslateBlock(BlockContent $block_content, $translations) {
    $languageFrom = $block_content->langcode->value;
    $fields = $block_content->getFields();
    $excludeFields = $this->getExcludeFields();
    $translatedTypes = $this->getTextFields();
    $config = \Drupal::config('auto_node_translate.config');
    $apiType = "Drupal\auto_node_translate\\" . $config->get('default_api');
    $api = new $apiType();

    foreach ($translations as $languageId => $value) {
      if ($value) {
        $block_content_trans = $this->getTransledBlock($block_content, $languageId);
        foreach ($fields as $field) {
          $fieldType = $field->getFieldDefinition()->getType();
          $fieldName = $field->getName();

          if (in_array($fieldType, $translatedTypes) && !in_array($fieldName, $excludeFields)) {
            $translatedValue = $this->translateTextField($field, $fieldType, $api, $languageFrom, $languageId);
            $block_content_trans->set($fieldName, $translatedValue);
          }
          elseif ($fieldType == 'link') {
            $values = $this->translateLinkField($field, $api, $languageFrom, $languageId);
            $block_content_trans->set($fieldName, $values);
          }
          elseif ($fieldType == 'entity_reference_revisions') {
            // Process later.
          }
          elseif (!in_array($fieldName, $excludeFields)) {
            $block_content_trans->set($fieldName, $block_content->get($fieldName)->getValue());
          }
        }
      }
    }

    foreach ($fields as $field) {
      $fieldType = $field->getFieldDefinition()->getType();
      if ($fieldType == 'entity_reference_revisions') {
        $this->translateParagraphField($field, $api, $languageFrom, $translations);
      }
    }

    $block_content->setNewRevision(TRUE);
    $block_content->revision_log = $this->t('Automatic translation using @api', ['@api' => $config->get('default_api')]);
    $block_content->setRevisionCreationTime(\Drupal::time()->getRequestTime());
    $block_content->setRevisionUserId(\Drupal::currentUser()->id());
    $block_content->save();
  }

  /**
   * Gets or adds translated block_content.
   *
   * @param mixed $block_content
   *   The block_content.
   * @param mixed $languageId
   *   The language id.
   *
   * @return mixed
   *   the translated block_content.
   */
  public function getTransledBlock(&$block_content, $languageId) {
    return $block_content->hasTranslation($languageId) ? $block_content->getTranslation($languageId) : $block_content->addTranslation($languageId);
  }

}
